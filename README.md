# python-scripts
Содержит скрипты на питоне

## Crypto
 - good_tokenomics_coins.py - ищет монеты с токеномикой, подходящей под заданные параметры
 - cmc_parser.py - парсит СМС, получает информацию о монетах и актуальных ценах
 - ar_premium_table_reader.py - скрипт парсинга таблицы AR Premium сообщества и мониторинга ситуации по монетам
 - cryptorank_alerts_automation.py - автоматизация создания алертов на cryptorank.io
 - coinstats_alerts_automation.py - автоматизация создания алертов на coinstats.app
