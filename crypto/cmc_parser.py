import time

import requests
from fake_useragent import UserAgent
from bs4 import BeautifulSoup

base_url = "https://coinmarketcap.com"
ua = UserAgent()
accept_header_value = "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;" \
                      "q=0.8,application/signed-exchange;v=b3;q=0.9"


def main():
    print('Getting supporting currencies list')
    time_start = time.time()
    currencies = get_currencies_list()
    time_end = time.time()
    print(f"Execution time {time_end - time_start}")


def get_currencies_list(symbols=None):
    print("Getting currencies")
    currencies = []
    headers = {
        'Accept': accept_header_value,
        'User-Agent': ua.random
    }
    current_page = 1
    while True:
        print(f'Getting page {current_page}')
        currencies_page = requests.get(url=f'{base_url}/?page={current_page}', headers=headers).text
        soup = BeautifulSoup(currencies_page, "lxml")
        table = soup.find('tbody')
        rows = table.findAll("tr")
        for row in rows:
            currency = parse_currency(row)
            if symbols is not None and len(symbols) >= 0:
                if currency['symbol'] in symbols:
                    symbols.remove(currency['symbol'])
                    currencies.append(currency)
                continue
            if currency['current_price'] != 0:
                currencies.append(currency)
        if symbols is not None and len(symbols) == 0:
            break
        print(f'Page {current_page} parsed successfully')
        if len(rows) < 100:
            break
        current_page += 1
    print(f"Successfully got {len(currencies)}")
    return currencies


def parse_currency(row):
    row_columns = row.findAll("td")
    currency_column = row_columns[2]
    href_url = currency_column.findAll('a')[0]['href']
    name_elements = currency_column.findAll("p")
    if len(name_elements) > 0:
        symbol = currency_column.find("p", class_="coin-item-symbol").text
    else:
        symbol = currency_column.find("span", class_="crypto-symbol").text
    current_price_text = row_columns[3].text
    try:
        current_price = float(current_price_text.replace("$", "").replace(",", ""))
    except ValueError:
        print(f"{symbol} unable parse price from {current_price_text}")
        current_price = 0
    currency = {
        'symbol': symbol,
        'href_url': href_url,
        'current_price': current_price
    }
    return currency


if __name__ == "__main__":
    main()
