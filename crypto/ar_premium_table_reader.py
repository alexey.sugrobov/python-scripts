import csv
import requests
import cmc_parser
from string import Template

symbol_replace_dict = {'WAX': 'WAXP', 'AURORY': 'AURORA'}
messasge_templates = {
    'buy': Template('Монета $symbol с текущей ценой $current_price '
                    'находится в зоне $order $zone. Покупка на $percents '
                    'от общего депозита'),
    'sell': Template('Монета $symbol с текущей ценой $current_price '
                    'находится в зоне $order $zone.')
}


def main():
    coins_dict = get_coins()
    coins_with_actual_prices = cmc_parser.get_currencies_list([key.strip() for key in coins_dict.keys()])
    result = define_zones(coins_dict, coins_with_actual_prices)
    print_results(result)


def print_results(result):
    for key in result.keys():
        coins_count = len(result[f"{key}"])
        if coins_count == 0:
            continue
        print(f'{key} количество монет {coins_count}')
        for coin in result[f'{key}']:
            print(messasge_templates[f'{coin["message_template_type"]}'].substitute(**coin))
        print('+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++')


def define_zones(coins_dict, coins_with_actual_prices):
    result = \
        {
            'Первая зона интереса': [],
            'Вторая зона интереса': [],
            'Третья зона интереса': [],
            'Первая зона продажи': [],
            'Вторая зона продажи': [],
            'Третья зона продажи': []
        }
    for coin in coins_with_actual_prices:
        coin_data = coins_dict[f"{coin['symbol']}"]
        if coin_data['second_order'] <= coin['current_price'] < coin_data['first_order']:
            append(result, symbol=coin['symbol'], key='Первая зона интереса', current_price=coin['current_price'],
                   order_name='первого ордера на покупку',
                   zone=f'{coin_data["first_order"]}-{coin_data["second_order"]}',
                   percents=coin_data["percents"], message_template_type='buy')
        elif coin_data['third_order'] <= coin['current_price'] < coin_data['second_order']:
            append(result, symbol=coin['symbol'], key='Вторая зона интереса', current_price=coin['current_price'],
                   order_name='второго ордера на покупку',
                   zone=f'{coin_data["second_order"]}-{coin_data["third_order"]}',
                   percents=coin_data["percents"], message_template_type='buy')
        elif coin['current_price'] <= coin_data['third_order']:
            append(result, symbol=coin['symbol'], key='Третья зона интереса', current_price=coin['current_price'],
                   order_name='третьего ордера на покупку',
                   zone=f'ниже {coin_data["third_order"]}',
                   percents=coin_data["percents"], message_template_type='buy')
        elif coin_data['first_sell'] <= coin['current_price'] < coin_data['second_sell']:
            append(result, symbol=coin['symbol'], key='Первая зона продажи', current_price=coin['current_price'],
                   order_name='первого ордера на продажу',
                   zone=f'{coin_data["first_sell"]}-{coin_data["second-sell"]}', message_template_type='sell')
        elif (coin_data['third_sell'] is not None and
              coin_data['second_sell'] <= coin['current_price'] < coin_data['third_sell']) \
                or (coin_data['second_sell'] <= coin['current_price']):
            append(result, symbol=coin['symbol'], key='Вторая зона продажи', current_price=coin['current_price'],
                   order_name='второго ордера на продажу',
                   zone=f'{coin_data["second_sell"]}-{coin_data["third_sell"]}', message_template_type='sell')
        elif coin_data['third_sell'] is not None and coin_data['third_sell'] <= coin['current_price']:
            append(result, symbol=coin['symbol'], key='Третья зона продажи', current_price=coin['current_price'],
                   order_name='третьего ордера на продажу',
                   zone=f'выше {coin_data["third_sell"]}', message_template_type='sell')
    return result


def append(result, key, symbol, current_price, order_name, zone, message_template_type, percents=None):
    result[f'{key}'].append(
        {
            'symbol': symbol,
            'current_price': current_price,
            'order': order_name,
            'zone': zone,
            'percents': percents,
            'message_template_type': message_template_type
        }
    )


def get_coins():
    coins = {}
    sheet_id = '1Uboeq26pDFGyXfuEd8PW2jn3eNCQ-zLXdCdYCHsBXs4'
    sheet_name = 'Портфель AR'

    url = f'https://docs.google.com/spreadsheets/d/{sheet_id}/export?format=csv&sheet={sheet_name}'
    result = requests.get(url)
    lines = result.text.splitlines()
    reader = csv.reader(lines)
    for row in reader:
        try:
            link_column_value = row[14]
            if len(link_column_value) == 0 or link_column_value.strip().lower() == 'coinmarketcap':
                continue
            symbol = row[1].strip()
            if symbol in symbol_replace_dict:
                symbol = symbol_replace_dict[f'{symbol}']
            coins[f'{symbol}'] = {
                'first_order': parse_price(row[6]),
                'second_order': parse_price(row[7]),
                'third_order': parse_price(row[8]),
                'first_sell': parse_price(row[9]),
                'second_sell': parse_price(row[10]),
                'third_sell': parse_price(row[11]),
                'percents': row[13]
            }
        except Exception as e:
            print(f"{row}, {e}")
    return coins


def parse_price(str):
    try:
        return float(str.replace("$", "").replace(",", ".").replace(u"\u00a0", "").replace("Â", ""))
    except ValueError as e:
        print(e)
        return None


if __name__ == "__main__":
    main()
