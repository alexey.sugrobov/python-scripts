import requests
import time
import pandas as pd
from datetime import datetime


def parse():
    result = []
    url = "https://api.cryptorank.io/v0/search"
    response = requests.get(url)
    data = response.json()
    coins = sorted(data['coins'], key=lambda c: (c['rank'] is None, c['rank']))
    for coin in coins:
        print(f"Getting coin data for {coin['name']}[{coin['symbol']}]")
        coin_url = f"https://api.cryptorank.io/v0/coins/{coin['key']}?locale=en"
        coin_response = requests.get(coin_url)
        coin_data = coin_response.json()['data']
        if coin_data['marketCap'] <= 50000000 and \
                coin_data['volume24h'] / coin_data['marketCap'] <= 0.03 and \
                coin_data['availableSupply'] / coin_data['totalSupply'] <= 0.5 and \
                coin_data['maxSupply'] <= 250000:
            tickers_url = f"https://api.cryptorank.io/v0/tickers?status=active&category=spot&isTickersForPriceCalc=true&isTickersForVolumeCalc=true&includeQuote=true&coinKeys={coin['key']}&exchangeKeys=okx,gateio,kucoin,mxc"
            ticker_response = requests.get(tickers_url)
            tickers = ticker_response.json()['data']
            exchanges = list(set(sub['exchangeName'] for sub in tickers))
            if len(exchanges) > 0:
                result.append({
                    'Name': coin['name'],
                    'Symbol': coin['symbol'],
                    'Market Cap': coin_data['marketCap'],
                    'Total Supply': coin_data['totalSupply'],
                    'Available Supply': coin_data['availableSupply'],
                    'Exchanges': ", ".join(exchanges)
                })
            print("stored")
        else:
            print("skip")
        time.sleep(1)
    print("Generating file")
    df = pd.DataFrame(result)
    writer = pd.ExcelWriter(f'Cryptorank_{datetime.now().strftime("%d/%m/%Y %H:%M:%S")}.xlsx')
    df.to_excel(writer, sheet_name='coins', index=False, na_rep='NaN')
    for column in df:
        column_length = max(df[column].astype(str).map(len).max(), len(column))
        col_idx = df.columns.get_loc(column)
        writer.sheets['coins'].set_column(col_idx, col_idx, column_length)
    writer.save()
    print("File generated")


if __name__ == '__main__':
    parse()
