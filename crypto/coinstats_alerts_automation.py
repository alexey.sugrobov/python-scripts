import random
import time
import logging
import requests
import json

from ar_premium_table_reader import get_coins


class CoinstatsAlertPublisher:
    __BASE_URL = 'https://api.coin-stats.com'
    __ALERTS_URL = f'{__BASE_URL}/v2/alerts'
    __COINS_URL = f'{__BASE_URL}/v4/coins'

    __coins_dict = {}
    __existing_alerts = {}
    __new_alerts = {}

    __LESS_THAN_CONDITION_TYPE = 1
    __GREATER_THAN_CONDITION_TYPE = 0

    def __init__(self, token, coins, comment_prefix, buy_order_fields, sell_order_fields, comment_messages):
        self.__logger = self.__configure_logger()
        self.__logger.info("Initializing...")
        self.__coins = coins
        self.__comment_prefix = comment_prefix
        self.__buy_order_fields = buy_order_fields
        self.__sell_order_fields = sell_order_fields
        self.__comment_messages = comment_messages
        self.__headers = {
            "authority": "api.coin-stats.com",
            "accept": "application/json, text/plain, */*",
            "accept-language": "ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7",
            "origin": "https://coinstats.app",
            "referer": "https://coinstats.app/",
            "user-agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/101.0.4951.64 Safari/537.36",
            "token": token
        }
        self.__logger.info("Initialization completed")

    def get_coins(self, limit=100):
        page_number = 0
        self.__logger.info("Getting coins...")
        coins_symbol = [key for key in self.__coins.keys()]
        while True:
            self.__logger.info(f"Getting page {page_number + 1}")
            payload = {
                'limit': limit,
                'skip': page_number * limit
            }
            coin_page = requests.get(self.__COINS_URL, params=payload, headers=self.__headers).json()
            self.__logger.info(f"Page {page_number + 1} successfully got")
            for coin in coin_page['coins']:
                if len(coins_symbol) == 0:
                    break
                current_symbol = coin['s']
                if current_symbol in self.__coins_dict:
                    continue
                if current_symbol in coins_symbol:
                    self.__coins_dict[current_symbol] = coin
                    coins_symbol.remove(current_symbol)
            if len(coin_page['coins']) < 100 or len(coins_symbol) == 0:
                break
            page_number += 1
            time.sleep(random.randrange(1, 2))
        self.__logger.info(f"{len(self.__coins_dict)} coins stored")

    def read_existing_alerts(self):
        self.__logger.info(f"Reading existing alerts with comment prefix {self.__comment_prefix}...")
        alerts_response = requests.get(self.__ALERTS_URL, headers=self.__headers).json()
        existing_alerts_dict = {}
        existing_alerts_counter = 0
        for alert in alerts_response['alerts']:
            symbol = alert['coin']['s']
            if symbol not in existing_alerts_dict:
                existing_alerts_dict[symbol] = {}
            if self.__comment_prefix and 'not' in alert and alert['note'] is not None \
                    and alert['note'].startswith(self.__comment_prefix):
                if alert['conditionType'] not in existing_alerts_dict[symbol]:
                    existing_alerts_dict[symbol][alert['conditionType']] = []
                existing_alerts_dict[symbol][alert['conditionType']].append(alert['priceChange'])
                existing_alerts_counter += 1
        self.__logger.info(f"Got {existing_alerts_counter} existing alerts")

    def define_new_alerts(self):
        for symbol, orders_data in self.__coins.items():
            self.__logger.info(f"Defining alerts for {symbol}")
            existing_alerts = None
            if symbol in self.__existing_alerts:
                existing_alerts = self.__existing_alerts[symbol]
            self.__add_new_alerts(symbol, orders_data, existing_alerts,
                                  self.__buy_order_fields, self.__LESS_THAN_CONDITION_TYPE)
            self.__add_new_alerts(symbol, orders_data, existing_alerts,
                                  self.__sell_order_fields, self.__GREATER_THAN_CONDITION_TYPE)
            self.__logger.info(f"All alerts for {symbol} defined")

    def __add_new_alerts(self, symbol, orders_data, existing_alerts, fields, alert_type):
        for field in fields:
            price = orders_data[field]
            if price is None or price == 0:
                self.__logger.info(f"Alert type {alert_type} for symbol {symbol} cause price not existing...")
                continue
            if existing_alerts is not None and price in existing_alerts[alert_type]:
                self.__logger.info(f"Alert type {alert_type}, price {price}, symbol {symbol} already exists. Skipping...")
                continue
            self.__logger.info(f"Adding alert for {symbol}, field {field}, price {price}")
            if symbol not in self.__new_alerts:
                self.__new_alerts[symbol] = []
            self.__new_alerts[symbol].append({
                'alert_type': alert_type,
                'price': price,
                'field': field
            })
            self.__logger.info(f"Added alert for {symbol}, field {field}, price {price}")

    def publish_alerts(self):
        coins_count = len(self.__new_alerts)
        if coins_count == 0:
            self.__logger.info("There is no alerts for publishing")
            return
        self.__logger.info(f"Publishing alerts for {coins_count} coins...")
        current_coin_index = 0
        failed_counter = 0
        for symbol, alerts in self.__new_alerts.items():
            current_coin_index += 1
            alerts_count = len(alerts)
            alert_index = 0
            self.__logger.info(f"Coin {current_coin_index}/{coins_count}, symbol {symbol}, alerts count {alerts_count}")
            coin = self.__coins_dict[symbol]
            for alert in alerts:
                alert_index += 1
                self.__logger.info(f"{symbol}: alert {alert_index}/{alerts_count}, "
                                   f"price {alert['price']}, alert type {alert['alert_type']}")
                if alert['field'] in self.__comment_messages:
                    comment_message = self.__comment_messages[alert['field']]
                else:
                    comment_message = alert['field']
                create_alert_request = {
                    'alertType': 0,
                    'conditionType': alert['alert_type'],
                    'frequencyType': 0,
                    'price': f'{coin["pu"]}',
                    'priceChange': f'{alert["price"]}',
                    'percentChange': "",
                    'coinSymbol': coin['s'],
                    'coinId': coin['i'],
                    'currency': 'USD',
                    'note': f'{self.__comment_prefix} {comment_message}'
                }
                result = requests.post(self.__ALERTS_URL, headers=self.__headers, json=create_alert_request)
                if result.status_code == 200:
                    self.__logger.info(f'{symbol}: alert {alert_index}/{alerts_count} published successfully')
                else:
                    failed_counter += 1
                    self.__logger.warning(f'{symbol}: alert {alert_index}/{alerts_count} fail')
                time.sleep(2)
            self.__logger.info(f"Published {alert_index}/{alerts_count} alerts for {symbol}")
            self.__logger.info(f"Published")
            if (failed_counter > 0):
                self.__logger.warning(f"{failed_counter} publishing alerts failed. See logs")

    def __configure_logger(self):
        formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
        logger = logging.getLogger("CoinstatsAlertPublisher")
        logger.setLevel(logging.INFO)
        console_handler = logging.StreamHandler()
        console_handler.setLevel(logging.INFO)
        console_handler.setFormatter(formatter)
        logger.addHandler(console_handler)
        return logger


def main():
    coins = get_coins()
    comment_messages = {
        'first_order': 'Цена достигла 1/3 ордера на покупку',
        'second_order': 'Цена достигла 2/3 ордера на покупку',
        'third_order': 'Цена достигла 3/3 ордера на покупку',
        'first_sell': 'Цена достигла 1/3 ордера на продажу',
        'second_sell': 'Цена достигла 2/3 ордера на продажу',
        'third_sell': 'Цена достигла 3/3 ордера на продажу'
    }
    coinstats_alert_publisher = CoinstatsAlertPublisher(token="r:f3d7b4c833db493f160b1beb06cf2cbc",
                                                        coins=coins, comment_prefix='[AR]',
                                                        buy_order_fields=['first_order', 'second_order', 'third_order'],
                                                        sell_order_fields=['first_sell', 'second_sell', 'third_sell'],
                                                        comment_messages=comment_messages)
    coinstats_alert_publisher.get_coins(limit=300)
    coinstats_alert_publisher.read_existing_alerts()
    coinstats_alert_publisher.define_new_alerts()
    coinstats_alert_publisher.publish_alerts()


if __name__ == '__main__':
    main()
