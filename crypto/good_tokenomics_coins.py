import sys

import requests
import time
import pandas as pd

ignored_coins = ['BTC', 'USDC', 'BUSD', 'TUSD', 'UST', 'TUSD', 'SUSD', 'USDK']
api_key = '20a61be8eaecf03ce555106892efc3c1c34e09c9'


def log_execution_time(func):
    def timed(*args, **kwargs):
        ts = time.time()
        print('Executing ', func.__name__)
        result = func(*args, **kwargs)
        te = time.time()
        print('Function', func.__name__, 'time:', round((te - ts) * 1000, 1), 'ms')
        return result

    return timed


@log_execution_time
def main(max_market_cap=sys.maxsize, max_vol_cap=0.03, max_circulating_percent=0.5,
         max_supply_limit=sys.maxsize, file_name='good_tokenomics_coins', platform_currency=''):
    currencies = []
    page = 0
    while True:
        page += 1
        data = get_page(page, platform_currency)
        currencies.extend(handle_data(data, max_market_cap, max_vol_cap, max_circulating_percent, max_supply_limit))
        if len(data) < 100:
            print('==============================')
            print(f'{len(currencies)} currencies loaded')
            break
        time.sleep(1)
    if len(currencies) > 1:
        write_to_file(currencies, file_name, platform_currency)
    else:
        print(f'There is no currencies with defined parameters: max market cap {max_market_cap}, max vol cap '
              f'{max_vol_cap}, max circulating percent {max_circulating_percent}, max supply limit {max_supply_limit}')


@log_execution_time
def handle_data(data, max_market_cap, max_vol_cap, max_circulating_percent, max_supply_limit):
    mapped_data = []
    stored_counter = 0
    for asset in data:
        if can_store(asset):
            continue
        volume_24h = float(asset['1d']['volume'])
        market_cap = float(asset['market_cap'])
        max_supply = float(asset['max_supply'])
        circulating_supply = float(asset['circulating_supply'])
        vol_cap = volume_24h / market_cap
        circulating_info = circulating_supply / max_supply
        price = float(asset['price'])
        ath = float(asset['high'])
        if volume_24h / market_cap < max_vol_cap or circulating_supply / max_supply < max_circulating_percent or \
                market_cap > max_market_cap or max_supply > max_supply_limit:
            continue
        mapped_data.append({'currency': asset['currency'],
                            'name': asset['name'],
                            'rank': asset['rank'],
                            'price': price,
                            'ath': ath,
                            'to_ath': f'{ath / price * 100}%',
                            'circulating_supply': circulating_supply,
                            'max_supply': max_supply,
                            'market_cap': market_cap,
                            'volume_24h': volume_24h,
                            'vol_cap': vol_cap,
                            'circulating_info': circulating_info
                            })
        stored_counter += 1
    print(f'Stored {stored_counter} currencies')
    return mapped_data


def can_store(asset):
    return 'circulating_supply' not in asset \
           or 'max_supply' not in asset \
           or 'market_cap' not in asset \
           or '1d' not in asset \
           or asset['currency'] in ignored_coins \
           or asset['market_cap'] == '0' \
           or asset['max_supply'] == '0'


@log_execution_time
def get_page(page, platform_currency):
    print(f'Getting page {page}')
    payload = {'key': api_key, 'page': page, 'convert': 'USD', 'interval': '1d', 'platform-currency': platform_currency}
    data = requests.get('https://api.nomics.com/v1/currencies/ticker', payload).json()
    print(f'Got {len(data)} currencies')
    return data


@log_execution_time
def write_to_file(currencies, file_name, platform_currency):
    df = pd.DataFrame(currencies)
    writer = pd.ExcelWriter(f'{file_name}{platform_currency}.xlsx')
    df.to_excel(writer, sheet_name='coins', index=False, na_rep='NaN')
    for column in df:
        column_length = max(df[column].astype(str).map(len).max(), len(column))
        col_idx = df.columns.get_loc(column)
        writer.sheets['coins'].set_column(col_idx, col_idx, column_length)
    writer.save()


if __name__ == '__main__':
    main(max_market_cap=250000, max_supply_limit=50000000)
