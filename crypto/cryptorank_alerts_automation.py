import os
import time
import logging
import requests

from ar_premium_table_reader import get_coins


class CryptoRankAlertPublisher:
    def authenticated(func):
        def wrapper(self, *args, **kwargs):
            if not self.__authenticated:
                raise CryptoRankAlertPublisher.CryptoRankException('You need login to perform action')
            return func(self, *args, **kwargs)
        return wrapper

    class CryptoRankException(Exception):
        pass

    __BASE_URL = 'https://api.cryptorank.io/'
    __LOGIN_URL = f'{__BASE_URL}v0/auth/password'
    __ALERTS_URL = f'{__BASE_URL}v0/user/alerts'
    __COINS_URL = f'{__BASE_URL}v0/coins?locale=en'

    __existing_alerts = {}
    __new_alerts = {}
    __coins_dict = {}

    def __init__(self, coins, buy_orders_fields, sell_orders_fields, comment_prefix, comment_messages={}):
        self.__logger = self.__configure_logger()
        self.__logger.info("Initializing...")
        self.__headers = {
            "%3Aauthority%3A": "api.cryptorank.io",
            "%3Amethod%3A": "POST",
            "%3Apath%3A": "/v0/auth/password",
            "%3Ascheme%3A": "https",
            "accept": "appication/json"
        }
        self.__authenticated = False
        self.__coins = coins
        self.__buy_order_fields = buy_orders_fields
        self.__sell_order_fields = sell_orders_fields
        self.__comment_prefix = comment_prefix
        self.__comment_messages = comment_messages
        self.__logger.info("Initialization completed")

    def __configure_logger(self):
        formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
        logger = logging.getLogger("CryptoRankAlertPublisher")
        logger.setLevel(logging.INFO)
        console_handler = logging.StreamHandler()
        console_handler.setLevel(logging.INFO)
        console_handler.setFormatter(formatter)
        logger.addHandler(console_handler)
        return logger

    def login(self, email, password):
        if self.__authenticated:
            self.__logger.warning(f'User {email} already authenticated')
            return
        self.__logger.info(f"Authenticated user {email}...")
        login_data = {
            'email': email,
            'password': password
        }
        login_response = requests.post(self.__LOGIN_URL, headers=self.__headers, data=login_data)
        if login_response.status_code != 201:
            raise CryptoRankAlertPublisher\
                .CryptoRankException(f"Unable login user {email}, cause {login_response.content}")
        self.__headers['Authorization'] = f'Bearer {login_response.json()["token"]}'
        self.__authenticated = True
        self.__logger.info(f"User {email} authenticated successfully")

    @authenticated
    def get_coins(self):
        self.__logger.info("Getting coins...")
        coins = requests.get(self.__COINS_URL, headers=self.__headers).json()
        coins_dict = {}
        coins_symbols = [key for key in self.__coins.keys()]
        for coin in coins['data']:
            if coin['symbol'] not in coins_symbols or \
                    coin['symbol'] in coins_dict:
                continue
            coins_dict[coin['symbol']] = coin
        self.__coins_dict = coins_dict
        self.__logger.info(f"Got {len(self.__coins_dict)} coins")

    @authenticated
    def read_existing_alerts(self):
        self.__logger.info(f"Reading existing alerts, with comment prefix {self.__comment_prefix}...")
        alerts = requests.get(self.__ALERTS_URL, headers=self.__headers).json()
        existing_alerts_dict = {}
        existing_alerts_counter = 0
        for alert in alerts['data']:
            symbol = alert['coin']['symbol']
            if symbol not in existing_alerts_dict:
                existing_alerts_dict[symbol] = {}
            if self.__comment_prefix and alert['comment'] is not None \
                    and alert['comment'].startswith(self.__comment_prefix):
                if alert['alertType'] not in existing_alerts_dict[symbol]:
                    existing_alerts_dict[symbol][alert['alertType']] = []
                existing_alerts_dict[symbol][alert['alertType']].append(alert['value1'])
                existing_alerts_counter += 1
        self.__existing_alerts = {key: value for key, value in existing_alerts_dict.items() if len(value) > 0}
        self.__logger.info(f"Got {existing_alerts_counter} existing alerts")

    def define_new_alerts(self):
        for symbol, orders_data in self.__coins.items():
            self.__logger.info(f"Defining alerts for {symbol}")
            existing_alerts = None
            if symbol in self.__existing_alerts:
                existing_alerts = self.__existing_alerts[symbol]
            self.__add_new_alerts(symbol, orders_data, existing_alerts, self.__buy_order_fields, 'PRICE_BELOW')
            self.__add_new_alerts(symbol, orders_data, existing_alerts, self.__sell_order_fields, 'PRICE_ABOVE')
            self.__logger.info(f"All alerts for {symbol} defined")

    @authenticated
    def publish_alerts(self):
        coins_count = len(self.__new_alerts)
        if coins_count == 0:
            self.__logger.info("There is not alerts for publishing")
            return
        self.__logger.info(f"Publishing alerts for {coins_count} coins...")
        current_coin_index = 0
        failed_counter = 0
        for symbol, alerts in self.__new_alerts.items():
            current_coin_index += 1
            alerts_count = len(alerts)
            alert_index = 0
            self.__logger.info(f"Coin {current_coin_index}/{coins_count}, symbol {symbol}, alerts count {alerts_count}")
            coin = self.__coins_dict[symbol]
            for alert in alerts:
                alert_index += 1
                self.__logger.info(f"{symbol}: alert {alert_index}/{alerts_count}, "
                                   f"price {alert['price']}, alert type {alert['alert_type']}")
                if alert['field'] in self.__comment_messages:
                    comment_message = self.__comment_messages[alert['field']]
                else:
                    comment_message = alert['field']
                create_signal_request = {
                    'type': 'add',
                    'alertType': alert['alert_type'],
                    'deliveryTg': True,
                    'currency': 'USD',
                    'coin': coin,
                    'coinKey': coin['key'],
                    'tickerId': None,
                    'exchangeKey': None,
                    'currentPrice': coin['price']['USD'],
                    'currentVolume': coin['volume24h'],
                    'ticker': None,
                    'value1': alert['price'],
                    'value2': None,
                    'priceFrom': 'index',
                    'comment': f'{self.__comment_prefix} {comment_message}',
                    'isShortcut': False
                }
                result = requests.post(self.__ALERTS_URL, headers=self.__headers, data=create_signal_request)
                if result.status_code == 201:
                    self.__logger.info(f'{symbol}: alert {alert_index}/{alerts_count} published successfully')
                else:
                    failed_counter += 1
                    self.__logger.warning(f'{symbol}: alert {alert_index}/{alerts_count} fail')
                time.sleep(2)
            self.__logger.info(f"Published {alert_index}/{alerts_count} alerts for {symbol}")
        self.__logger.info(f"Published")
        if (failed_counter > 0):
            self.__logger.warning(f"{failed_counter} publishing alerts failed. See logs")

    def __add_new_alerts(self, symbol, orders_data, existing_alerts, fields, alert_type):
        for field in fields:
            price = orders_data[field]
            if price is None or price == 0:
                self.__logger.info(f"Alert type {alert_type} for symbol {symbol} cause price not existing...")
                continue
            if existing_alerts is not None and price in existing_alerts[alert_type]:
                self.__logger.info(f"Alert type {alert_type}, price {price}, symbol {symbol} already exists. Skipping...")
                continue
            self.__logger.info(f"Adding alert for {symbol}, field {field}, price {price}")
            if symbol not in self.__new_alerts:
                self.__new_alerts[symbol] = []
            self.__new_alerts[symbol].append({
                'alert_type': alert_type,
                'price': price,
                'field': field
            })
            self.__logger.info(f"Added alert for {symbol}, field {field}, price {price}")


def main():
    email = os.getenv("EMAIL", "email")
    password = os.getenv("PASSWORD", "password")
    comment_messages = {
        'first_order': 'Цена достигла 1/3 ордера на покупку',
        'second_order': 'Цена достигла 2/3 ордера на покупку',
        'third_order': 'Цена достигла 3/3 ордера на покупку',
        'first_sell': 'Цена достигла 1/3 ордера на продажу',
        'second_sell': 'Цена достигла 2/3 ордера на продажу',
        'third_sell': 'Цена достигла 3/3 ордера на продажу'
    }
    cryptorank_alert_publisher = CryptoRankAlertPublisher(get_coins(),
                                                          ['first_order', 'second_order', 'third_order'],
                                                          ['first_sell', 'second_sell', 'third_sell'], '[AR]',
                                                          comment_messages)
    cryptorank_alert_publisher.login(email, password)
    cryptorank_alert_publisher.get_coins()
    cryptorank_alert_publisher.read_existing_alerts()
    cryptorank_alert_publisher.define_new_alerts()
    cryptorank_alert_publisher.publish_alerts()


if __name__ == '__main__':
    main()
