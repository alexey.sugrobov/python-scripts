from prettytable import PrettyTable

def main():
    try:
        coin = input('Enter coin name: ')
        deposit = float(input('Enter your deposit in dollars: '))
        deal_percent = int(input('Enter deal percent from deposit: '))
        deal_size = deposit * deal_percent / 100
        leverage = int(input('Enter deal leverage: '))
        fibo_start_price = float(input('Enter fibo start price: '))
        fibo_end_price = float(input('Enter fibo end price: '))
        type_ = 'LONG' if fibo_end_price > fibo_start_price else 'SHORT'
        fibo0236 = fibo_end_price - (fibo_end_price - fibo_start_price) * 0.236
        fibo0382 = fibo_end_price - (fibo_end_price - fibo_start_price) * 0.382
        fibo05 = fibo_end_price - (fibo_end_price - fibo_start_price) * 0.5
        fibo0618 = fibo_end_price - (fibo_end_price - fibo_start_price) * 0.618
        fibo0786 = fibo_end_price - (fibo_end_price - fibo_start_price) * 0.786
        first_bucket_data = {
            'Number': 1,
            'Limit order': round(fibo0382 + ((fibo0236 - fibo0382) * 0.15), 2),
            'TP': round(fibo0236 - ((fibo0236 - fibo0382) * 0.15), 2),
            'SL': round(fibo0382 - ((fibo0382 - fibo05) * 0.15), 2),
        }
        second_bucket_data = {
            'Number': 2,
            'Limit order': round(fibo05 + ((fibo0382 - fibo05) * 0.15), 2),
            'TP': round(fibo0382 - ((fibo0382 - fibo05) * 0.15), 2),
            'SL': round(fibo05 - ((fibo05 - fibo0618) * 0.15), 2),
        }
        third_bucket_data = {
            'Number': 3,
            'Limit order': round(fibo0618 + ((fibo05 - fibo0618) * 0.15), 2),
            'TP': round(fibo05 - ((fibo05 - fibo0618) * 0.15), 2),
            'SL': round(fibo0618 - ((fibo0618 - fibo0786) * 0.15), 2),
        }
        deal_sum = deal_size * leverage
        first_order_quantity = round(deal_sum * 0.2 / first_bucket_data['Limit order'], 2)
        second_order_quantity = round(deal_sum * 0.3 / second_bucket_data['Limit order'], 2)
        third_order_quantity = round(deal_sum * 0.5 / third_bucket_data['Limit order'], 2)
        table = PrettyTable()
        print()
        print(f'Coin: {coin} | Depo: {deposit} | Deal percent {deal_percent}% | Leverage: x{leverage} | '
              f'Deal size {deal_size} | Type: {type_}')
        print()
        table.field_names = ["Number", "Limit Order", "Coins count", "Take profit", "Stop loss"]
        table.add_row([first_bucket_data['Number'], first_bucket_data['Limit order'], first_order_quantity,
                       first_bucket_data['TP'], first_bucket_data['SL']])
        table.add_row([second_bucket_data['Number'], second_bucket_data['Limit order'], second_order_quantity,
                       second_bucket_data['TP'], second_bucket_data['SL']])
        table.add_row([third_bucket_data['Number'], third_bucket_data['Limit order'], third_order_quantity,
                       third_bucket_data['TP'], third_bucket_data['SL']])
        print(table)
    except Exception as e:
        print(f"Wrong input: {e}")


if __name__ == '__main__':
    main()
